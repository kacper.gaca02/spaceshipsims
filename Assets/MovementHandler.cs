using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementHandler : MonoBehaviour
{
    public static MovementHandler Instance;
    public List<GameObject> targets;
    public bool canMove { get; set; }

    private void Awake()
    {
        Instance = this;
        canMove = true;
    }


    
}
