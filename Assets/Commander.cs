using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static UnityEngine.GraphicsBuffer;

public class Commander : MonoBehaviour
{
    public NavMeshAgent agent;
    private Animator _animator;
    public GameObject backupTarget, gotoTarget;
    public GameObject alienSalute;
    int time;

    private void Awake()
    {
        agent = GetComponent<NavMeshAgent>();
        _animator = GetComponent<Animator>();
    }

    private void Start()
    {
        StartCoroutine(OrderTimeCalculator());
    }

    IEnumerator OrderTimeCalculator()
    {
        //Debug.Log("OrderTimeCalculator() started");
        time = Random.Range(10,15);
        while (true)
        {
            yield return new WaitForSeconds(time);
            agent.SetDestination(gotoTarget.transform.position);
            _animator.Play("generalwalking");
            break;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("goto"))
        {
            StopCoroutine(OrderTimeCalculator());
            MovementHandler.Instance.targets.Insert(0, alienSalute);
        }
        if (other.gameObject.CompareTag("backup")) StartCoroutine(OrderTimeCalculator());
    }
}
