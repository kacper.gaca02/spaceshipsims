using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using static UnityEngine.GraphicsBuffer;

public class Room : MonoBehaviour
{
    public Bar bar;
    public Alien alien;
    public Commander commander;
    public GameObject target;
    public string animationName;
    public float time;
    public GameObject progressBarObj;
    public GameObject progressBar;
    private AudioSource _audiosource;
    [SerializeField] private Sprite progressBarImage;

    private void Awake()
    {
        _audiosource = GetComponent<AudioSource>();
    }

    public void Refill()
    {
        MovementHandler.Instance.canMove = false;
        alien.GetComponent<Animator>().Play(animationName);
        progressBarObj.SetActive(true);
        progressBarObj.GetComponent<Image>().sprite = progressBarImage;
        if (_audiosource) _audiosource.Play();
        StartCoroutine(ProgressBar());
        StartCoroutine(Timer());
        if (target)
        {
            Invoke("RotateAlien", .2f);
        }
    }

    IEnumerator Timer()
    {
        while (true)
        {
            if (target != null) alien.transform.LookAt(target.transform);
            yield return new WaitForSeconds(time);
            if(bar != null) bar.Refill();
            MovementHandler.Instance.canMove = true;
            alien.gameObject.GetComponent<NavMeshAgent>().isStopped = false;
            if (bar == null && commander != null) commander.agent.SetDestination(commander.backupTarget.transform.position);
            if (_audiosource) _audiosource.Stop();
            StopCoroutine(ProgressBar());
            StopCoroutine(Timer());
            progressBar.transform.localScale = Vector3.one;
            progressBarObj.SetActive(false);
            break;
        }
    }
    IEnumerator ProgressBar()
    {
        int attempt = 1;
        while (true)
        {
            yield return new WaitForSeconds(1);
            attempt++;
            progressBar.transform.localScale = new Vector3(
                progressBar.transform.localScale.x - (1/time),
                progressBar.transform.localScale.y,
                progressBar.transform.localScale.z);
            if (attempt >= time) break;
        }
    }
    void RotateAlien()
    {
        alien.transform.LookAt(target.transform);
        alien.gameObject.GetComponent<NavMeshAgent>().isStopped = true;      
    }
}
