using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Alien : MonoBehaviour
{
    private NavMeshAgent _agent;
    private Animator _animator;
    private AudioSource _audioSource;
    private bool soundDebounce;
    public Vector3 alienDestination;
    List<GameObject> _targets;
    [SerializeField]string _animationName;
    private void Awake()
    {
        soundDebounce = true;
        _agent = GetComponent<NavMeshAgent>();
        _targets = MovementHandler.Instance.targets;
        _animator = GetComponent<Animator>();
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (MovementHandler.Instance.targets.Count >= 1)
        {
            if (MovementHandler.Instance.canMove)
            {
                _agent.SetDestination(_targets[0].transform.position);
                _animator.Play("walking");
                if (soundDebounce)
                {
                    _audioSource.Play();
                    soundDebounce= false;
                }
                if (Vector3.Distance(transform.position, _targets[0].transform.position) <= 0.5f)
                {
                    //_animator.Play(_animationName);
                    _audioSource.Stop();
                    soundDebounce = true;
                    _targets[0].transform.GetComponent<Room>().Refill();
                    _targets.Remove(_targets[0]);
                }
            }
        }
    }

}
