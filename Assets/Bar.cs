using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bar : MonoBehaviour
{
    [SerializeField] public GameObject target;
    public float speed;
    float size = 1;
    bool signalDebounce = true;
    bool refillDebounce;
    public void SendTarget()
    {
        MovementHandler.Instance.targets.Add(target.gameObject);
        refillDebounce = true;
    }

    private void Start()
    {
        StartCoroutine(BarCollapse());
    }

    public void Refill()
    {
        if (refillDebounce)
        {
            transform.localScale = Vector3.one;
            size = 1;
            signalDebounce = true;
            refillDebounce= false;
            MovementHandler.Instance.canMove = true;
        }
    }

    IEnumerator BarCollapse()
    {
        while (true)
        {
            if (size > 0)
            {
                yield return new WaitForSeconds(2f);
                if (size > speed/100) size -= speed/100;
                else size -= size;
                transform.localScale = new Vector3(size, 1, 1);
                if (size <= .8f && signalDebounce)
                {
                    SendTarget();
                    signalDebounce = false;
                }
            }
            else { yield return null; }
        }
    }
}
